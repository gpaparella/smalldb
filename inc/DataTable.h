#ifndef DATATABLE_H
#define DATATABLE_H

#include <string>
#include <vector>

#ifndef DATATYPES_H
#include "DataTypes.h"
#endif

class DataTable
{
	//typedef struct {

	//	char Stb[64];      // set-top-box id on which the media asset was viewed
	//	char Title[64];    // title of the media asset
	//	char Provider[64]; // distributor of the media asset
	//	TableDate Date;	   // local date on which the content was leased through the STB
	//	unsigned int Rev;		   // price in cents to lease the asset
	//	unsigned int ViewTime;	   // amount of time the STB played the asset, in minutes

	//} Row;

	// column flags:
	static const unsigned int COL_STB      = 0x000001;
	static const unsigned int COL_TITLE    = 0x000010;
	static const unsigned int COL_PROVIDER = 0x000100;
	static const unsigned int COL_DATE     = 0x001000;
	static const unsigned int COL_REV      = 0x010000;
	static const unsigned int COL_VIEWTIME = 0x100000;

	// column labels:
	const std::string COLLABEL_STB; // = "STB";
	const std::string COLLABEL_TITLE; // = "TITLE";
	const std::string COLLABEL_PROVIDER; // = "PROVIDER";
	const std::string COLLABEL_DATE; // = "DATE";
	const std::string COLLABEL_REV; // = "REV";
	const std::string COLLABEL_VIEWTIME; // = "VIEW_TIME";

	static const size_t RowSize = sizeof(Row);

	std::string InputFileName;

	const std::string TableFileName; // = "DataTable.dat";
	std::fstream* TableStream;

public:
	DataTable();
	DataTable(std::string fileName);
	~DataTable();

	Row* ImportLine(std::string line);
	void SerializeRow(char* output, Row* row);
	void DeserializeRow(char* input, Row* row);
	void CreateFromFile();
	void ImportFile(std::string fileName);

	// return datastore table offsets for matching rows
	std::vector<long> FindRowOffsets(Row* row, unsigned int filterFlags);

	// return matching rows
	std::vector<Row> FindRows(Row* row, unsigned int filterFlags);

	std::string Query(std::string select, std::string order, std::string filter);

private:
	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
	std::vector<std::string>  split(const std::string &s, char delim);

	void ResetStream();

	long CheckIfKeyExists(std::string stb, std::string title, TableDate date);
	bool CheckIfRowMatchesFilter(Row* filterRow, Row* tableRow, unsigned int filter);
};


#endif
