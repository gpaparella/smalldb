#ifndef DATATYPES_H
#define DATATYPES_H

struct TableDate {

	int year;  // year since 1900
	int month; // month of year from 0 to 11
	int day;   // day of month from 1 to 31

	bool operator<(const TableDate& rhs) const
	{
		if (year < rhs.year)
		{
			return true;
		}
		else if (year == rhs.year)
		{
			if (month < rhs.month)
			{
				return true;
			}
			else if (month == rhs.month)
			{
				return day < rhs.day;
			}
		}

		return false;
	}

	bool operator==(const TableDate& rhs)
	{
		return (year == rhs.year) && 
			   (month == rhs.month) && 
			   (day == rhs.day);
	}

};

struct Row {

	char Stb[64];      // set-top-box id on which the media asset was viewed
	char Title[64];    // title of the media asset
	char Provider[64]; // distributor of the media asset
	TableDate Date;	   // local date on which the content was leased through the STB
	unsigned int Rev;		   // price in cents to lease the asset
	unsigned int ViewTime;	   // amount of time the STB played the asset, in minutes

};

#endif
