//#include "stdafx.h"

#ifndef DATATABLE_H
#include "DataTable.h"
#endif

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <algorithm>

int StbLt(const Row lhs, const Row rhs)
{
	return (lhs.Stb < rhs.Stb);
}
int TitleLt(const Row lhs, const Row rhs)
{
	return (lhs.Title < rhs.Title);
}
int ProviderLt(const Row lhs, const Row rhs)
{
	return (lhs.Provider < rhs.Provider);
}
int DateLt(const Row lhs, const Row rhs)
{
	return (lhs.Date < rhs.Date);
}
int RevLt(const Row lhs, const Row rhs)
{
	return (lhs.Rev < rhs.Rev);
}
int ViewTimeLt(const Row lhs, const Row rhs)
{
	return (lhs.ViewTime < rhs.ViewTime);
}


using namespace std;

	DataTable::DataTable() : COLLABEL_STB("STB")
                               , COLLABEL_TITLE("TITLE")
                               , COLLABEL_PROVIDER("PROVIDER")
                               , COLLABEL_DATE("DATE")
                               , COLLABEL_REV("REV")
                               , COLLABEL_VIEWTIME("VIEW_TIME")
                               , TableFileName("./DataTable.dat")
                               , TableStream(0)
	{
		ResetStream();
	}

	DataTable::DataTable(string fileName) : InputFileName(fileName)
	                                      , COLLABEL_STB("STB")
                                              , COLLABEL_TITLE("TITLE")
                                              , COLLABEL_PROVIDER("PROVIDER")
                                              , COLLABEL_DATE("DATE")
                                              , COLLABEL_REV("REV")
                                              , COLLABEL_VIEWTIME("VIEW_TIME")
                                              , TableFileName("./DataTable.dat")
                                              , TableStream(0)
	{
	}

	DataTable::~DataTable()
	{
            if (TableStream != 0)
            {
                delete(TableStream);
                TableStream = 0;
            }
	}

	Row* DataTable::ImportLine(string line)
	{
		Row* row = new Row();

		vector<string> fields = split(line, '|');
		
		if (fields.size() != 6) {
			// error!
		}

		vector<string> ymdTokens = split(fields[3], '-');
		vector<string> dollarCentTokens = split(fields[4], '.');
		vector<string> minuteSecondTokens = split(fields[5], ':');

		if ((ymdTokens.size() != 3) ||
	            (dollarCentTokens.size() != 2) ||
	            (minuteSecondTokens.size() != 2)) {
			// error!
		}

		strcpy(row->Stb,      fields[0].c_str());
		strcpy(row->Title,    fields[1].c_str());
		strcpy(row->Provider, fields[2].c_str());

		std::string::size_type sz;

		TableDate td{ 
                             std::stoi(ymdTokens[0],&sz) - 1900, // year
                             std::stoi(ymdTokens[1],&sz) - 1,    // month
                             std::stoi(ymdTokens[2],&sz)         // day
		            };

		row->Date = td;

		unsigned int dollars = stoi(dollarCentTokens[0]);
		unsigned int cents   = stoi(dollarCentTokens[1]);

		row->Rev = (dollars * 100) + cents;

		unsigned int minutes = stoi(minuteSecondTokens[0]);
		unsigned int seconds = stoi(minuteSecondTokens[1]);

		row->ViewTime = (minutes * 60) + seconds;

		return row;
	}

	void DataTable::SerializeRow(char* output, Row* row) {

		memcpy(output, row, RowSize);

	}

	void DataTable::DeserializeRow(char* input, Row* row) {

		// TODO: replace hard-coded offsets with constant expressions

		strcpy(row->Stb,      &input[0]);
		strcpy(row->Title,    &input[64]);
		strcpy(row->Provider, &input[128]);

		memcpy(&(row->Date), &input[192], sizeof(TableDate));
		memcpy(&(row->Rev), &input[204], sizeof(unsigned int));
		memcpy(&(row->ViewTime), &input[208], sizeof(unsigned int));
	}

	// perform whole-table scan
	std::vector<long> DataTable::FindRowOffsets(Row* row, unsigned int filters) {

		std::vector<long> rowsFound;

		TableStream->seekg(0, TableStream->beg);
		TableStream->seekg(0, TableStream->end);
		long length = TableStream->tellg();

		if (length == -1) {
			ResetStream();
			TableStream->seekg(0, TableStream->end);
			length = TableStream->tellg();
		}

		TableStream->seekg(0, TableStream->beg);

		char* rowData = new char[RowSize];
		Row currRow;

		long offset = 0;
		while (offset < length)
		{
			TableStream->read(rowData, RowSize);

			DeserializeRow(rowData, &currRow);

			bool match = CheckIfRowMatchesFilter(row, &currRow, filters);

			if (match) {
				rowsFound.push_back(offset);
			}

			offset += RowSize;
		}

		delete[] rowData;

		return rowsFound;
	}

	// perform whole-table scan
	std::vector<Row> DataTable::FindRows(Row* row, unsigned int filters) {

		std::vector<Row> rowsFound;

		TableStream->seekg(0, TableStream->beg);
		TableStream->seekg(0, TableStream->end);
		long length = TableStream->tellg();

		if (length == -1) {
			ResetStream();
			TableStream->seekg(0, TableStream->end);
			length = TableStream->tellg();
		}

		TableStream->seekg(0, TableStream->beg);

		char* rowData = new char[RowSize];
		Row currRow;

		long offset = 0;
		while (offset < length)
		{
			TableStream->read(rowData, RowSize);

			DeserializeRow(rowData, &currRow);

			bool match = CheckIfRowMatchesFilter(row, &currRow, filters);

			if (match) {
				rowsFound.push_back(currRow);
			}

			offset += RowSize;
		}

		delete[] rowData;

		return rowsFound;
	}

	//DataTable::Row DataTable::GetRow(long pos)
	//{
	//	OpenReader();
	//	Row* row = new Row();
	//	FileReader->seekg(0, FileReader->beg);
	//	FileReader->seekg(pos);
	//	char* buffer = new char[RowSize];
	//	FileReader->read(buffer, RowSize);

	//	delete(buffer);

	//	return *row;
	//}

	void DataTable::CreateFromFile()
	{
		char* rowbuffer = 0;
		ofstream outfile;
		ifstream infile;

		try {

			ifstream infile(InputFileName.c_str());
			if (!infile.is_open()) {
				//std::cout << "unable to load file" << std::endl;
				return;
			}

			ofstream outfile(TableFileName, ios::out | ios::binary);

			string str;
			rowbuffer = (char*)malloc(RowSize);

			const string headerRow = "STB|TITLE|PROVIDER|DATE|REV|VIEW_TIME";
			const size_t headerRowSz = headerRow.length();

			if (getline(infile, str))
			{
				if (strncmp(str.c_str(), headerRow.c_str(), headerRowSz) != 0)
				{
					Row* row = ImportLine(str);
					SerializeRow(rowbuffer, row);
					outfile.write(rowbuffer, RowSize);
					delete(row);
				}
			}

			while (getline(infile, str))
			{
				Row* row = ImportLine(str);
				SerializeRow(rowbuffer, row);
				outfile.write(rowbuffer, RowSize);
				delete(row);
			}

			outfile.close();
			infile.close();

			free(rowbuffer);
			rowbuffer = 0;
		}
		catch (...) 
		{
			if (rowbuffer != 0) {
				free(rowbuffer);
				rowbuffer = 0;
			}

			if (outfile.is_open()) {
				outfile.close();
			}

			if (infile.is_open()) {
				infile.close();
			}
		}
	}

	void DataTable::ImportFile(string fileName) {

		char* rowbuffer = 0;
		ifstream infile;

		try {

			ifstream infile(fileName.c_str());
			if (!infile.is_open()) {
				//std::cout << "unable to load file" << std::endl;
				return;
			}

			ResetStream();

			string str;
			rowbuffer = (char*)malloc(RowSize);

			const string headerRow = "STB|TITLE|PROVIDER|DATE|REV|VIEW_TIME";
			const size_t headerRowSz = headerRow.length();

			// first pass performs updates only:

			vector<int> unmatchedIndices;

			if (getline(infile, str))
			{
				if (strncmp(str.c_str(), headerRow.c_str(), headerRowSz) != 0)
				{
					Row* row = ImportLine(str);
					long offset = CheckIfKeyExists(row->Stb, row->Title, row->Date);

					// if found in table, update row
					if (offset != -1) {
						TableStream->seekp(offset);
						SerializeRow(rowbuffer, row);
						TableStream->write(rowbuffer, RowSize);
						TableStream->flush();
					}
					else {
						unmatchedIndices.push_back(0);
					}

					delete(row);
				}
			}

			int rowIndx = 1;

			while (getline(infile, str))
			{
				Row* row = ImportLine(str);
				long offset = CheckIfKeyExists(row->Stb, row->Title, row->Date);

				// if found in table, update row
				if (offset != -1) {
					TableStream->seekp(offset);
					SerializeRow(rowbuffer, row);
					TableStream->write(rowbuffer, RowSize);
					TableStream->flush();
				}
				else {
					unmatchedIndices.push_back(rowIndx);
				}

				delete(row);
				rowIndx++;
			}

			if (unmatchedIndices.size() == 0) {
				return;
			}

			// second pass performs appends

			TableStream->close();
			ofstream appendfile(TableFileName, ios::out | ios::app);

			infile.close();
			infile.open(fileName.c_str(), ios::in);

			// if the input file does not contain a header row, rewind to the
			// beginning again.
			if (getline(infile, str))
			{
				if (strncmp(str.c_str(), headerRow.c_str(), headerRowSz) != 0)
				{
					infile.close();
					infile.open(fileName.c_str(), ios::in);
				}
			}

			int nextIndx = unmatchedIndices[0];

			// advance to the first line that has a row to append
			for (int i = 1; i <= nextIndx; i++) {
				getline(infile, str);
			}

			for (unsigned int i = 0; i < unmatchedIndices.size(); i++) {

				Row* row = ImportLine(str);
				
				appendfile.seekp(0, ios_base::end);
				SerializeRow(rowbuffer, row);
				appendfile.write(rowbuffer, RowSize);
				appendfile.flush();

				int lastIndx = nextIndx;
				int nextIndx = unmatchedIndices[i];
				int rowsToAdvance = nextIndx - lastIndx;

				// advance to next row that has a row to append
				for (int j = 0; j < rowsToAdvance; j++) {
					getline(infile, str);
				}

				delete(row);
			}

			free(rowbuffer);
			rowbuffer = 0;
		}
		catch (...)
		{
			if (rowbuffer != 0) {
				free(rowbuffer);
				rowbuffer = 0;
			}

			if (TableStream->is_open()) {
				TableStream->close();
			}
		}

	}

	std::string DataTable::Query(std::string select, std::string order, std::string filter)
	{
		//std::cout << "In DataTable::Query method\n";

		vector<string> filterFields = split(filter, '=');
		string colName = filterFields[0];
		string colVal = filterFields[1];
		unsigned int colfilter;
		Row testRow;

		if (colName == COLLABEL_STB) {

			colfilter = COL_STB;
			strcpy(testRow.Stb, colVal.c_str());
			
		} else if (colName == COLLABEL_TITLE) {

			colfilter = COL_TITLE;
			strcpy(testRow.Title, colVal.c_str());

		} else if (colName == COLLABEL_PROVIDER) {

			colfilter = COL_PROVIDER;
			strcpy(testRow.Provider, colVal.c_str());

		} else if (colName == COLLABEL_DATE) {

			colfilter = COL_DATE;
			vector<string> ymdTokens = split(colVal, '-');
			TableDate td{
				stoi(ymdTokens[0]) - 1900,  // year
				stoi(ymdTokens[1]) - 1,     // month
				stoi(ymdTokens[2])          // day
			};

			testRow.Date = td;

		} else if (colName == COLLABEL_REV) {

			colfilter = COL_REV;
			vector<string> dollarCentTokens = split(colVal, '.');
			unsigned int dollars = stoi(dollarCentTokens[0]);
			unsigned int cents = stoi(dollarCentTokens[1]);
			testRow.Rev = (dollars * 100) + cents;

		} else if (colName == COLLABEL_VIEWTIME) {

			colfilter = COL_VIEWTIME;
			vector<string> minuteSecondTokens = split(colVal, ':');
			unsigned int minutes = stoi(minuteSecondTokens[0]);
			unsigned int seconds = stoi(minuteSecondTokens[1]);
			testRow.ViewTime = (minutes * 60) + seconds;
		}

		std::vector<Row> rowsFound = FindRows(&testRow, colfilter);

		// It looks like the qsort that is available here is destructive to the order of elements
		// which evaluate to equal.  So I cannot simply recursively sort, starting with the least
		// significant ordering criterion.
		vector<string> orderFields = split(order, ',');
		if (orderFields.size() > 0)
		{
			for (int i = orderFields.size() - 1; i >= 0; i--) {
				string colName = orderFields[i];

				int lastIndx = (int)(rowsFound.size() - 1);

				if (colName == "STB") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), StbLt);

				}
				else if (colName == "TITLE") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), TitleLt);

				}
				else if (colName == "PROVIDER") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), ProviderLt);

				}
				else if (colName == "DATE") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), DateLt);

				}
				else if (colName == "REV") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), RevLt);

				}
				else if (colName == "VIEW_TIME") {

					std::stable_sort(rowsFound.begin(), rowsFound.end(), ViewTimeLt);
				}

			}

/*
			auto sortingLambda = [orderFields](Row first, Row second)
			{
				bool lt = true;
				bool eq = false;
				for (int i = 0; i < orderFields.size(); i++) {

					string colName = orderFields[i];
					if (colName == "STB") {

						eq = first.Stb == second.Stb;
						lt = first.Stb <  second.Stb;

					}
					else if (colName == "TITLE") {

						eq = first.Title == second.Title;
						lt = first.Title <  second.Title;

					}
					else if (colName == "PROVIDER") {

						eq = first.Provider == second.Provider;
						lt = first.Provider <  second.Provider;

					}
					else if (colName == "DATE") {

						eq = first.Date == second.Date;
						lt = first.Date <  second.Date;

					}
					else if (colName == "REV") {

						eq = first.Rev == second.Rev;
						lt = first.Rev <  second.Rev;

					}
					else if (colName == "VIEW_TIME") {

						eq = first.ViewTime == second.ViewTime;
						lt = first.ViewTime <  second.ViewTime;
					}
					
					if (!eq) {
						break;
					}
				}

				return eq ? 0 : (lt ? -1 : 1);
			};

			std::qsort(&rowsFound[0], rowsFound.size(), RowSize, sortingLambda);*/
		}

		vector<string> selectFields = split(select, ',');
		stringstream output;
		for (unsigned int i = 0; i < rowsFound.size(); i++) {

			Row currRow = rowsFound[i];

			for (unsigned int j = 0; j < selectFields.size(); j++) {

				if (j > 0) {
					output << ",";
				}

				string colName = selectFields[j];
				if (colName == "STB") {

					output << currRow.Stb;

				}
				else if (colName == "TITLE") {

					output << currRow.Title;

				}
				else if (colName == "PROVIDER") {

					output << currRow.Provider;

				}
				else if (colName == "DATE") {

					output << (currRow.Date.year+1900) << "-" << 
						  (currRow.Date.month+1) << "-" << 
                                                  currRow.Date.day;

				}
				else if (colName == "REV") {

					int cents = currRow.Rev % 100;
					output << currRow.Rev / 100 << "." <<
						  ((cents < 10) ? "0" : "") << cents;

				}
				else if (colName == "VIEW_TIME") {

					int minutes = currRow.ViewTime % 60;
					output << currRow.ViewTime / 60 << ":" <<
						  ((minutes < 10) ? "0" : "") << minutes;
				}

			}

			output << "\n";
		}

		return output.str();
	}

	vector<string>& DataTable::split(const string &s, char delim, vector<string> &elems)
	{
		stringstream ss(s);
		string item;
		while (getline(ss, item, delim)) {
			elems.push_back(item);
		}
		return elems;
	}

	vector<string>  DataTable::split(const string &s, char delim)
	{
		vector<string> elems;
		split(s, delim, elems);
		return elems;
	}

	void DataTable::ResetStream()
	{
		if (TableStream != 0) {
			if (TableStream->is_open()) {
				TableStream->close();
			}
			delete(TableStream);
		}

		TableStream = new fstream(TableFileName.c_str(), ios::in | ios::out | ios::binary);
	}

	long DataTable::CheckIfKeyExists(string stb, string title, TableDate date) {

		Row testRow;
		strcpy(testRow.Stb, stb.c_str());
		strcpy(testRow.Title, title.c_str());
		testRow.Date.year = date.year;
		testRow.Date.month = date.month;
		testRow.Date.day = date.day;

		unsigned int rowFilter = COL_STB | COL_TITLE | COL_DATE;

		vector<long> found = FindRowOffsets(&testRow, rowFilter);
		if (found.size() == 0) {
			return -1;
		}
		else {
			return found[0];
		}
	}

	bool DataTable::CheckIfRowMatchesFilter(Row* filterRow, Row* tableRow, unsigned int filter)
	{
		bool match = true;
		if (((filter & COL_STB) != 0x0) &&
			(strncmp(filterRow->Stb, tableRow->Stb, 64) != 0))
		{
			match = false;
		}
		if (match &&
			((filter & COL_TITLE) != 0x0) &&
			(strncmp(filterRow->Title, tableRow->Title, 64) != 0))
		{
			match = false;
		}
		if (match &&
			((filter & COL_PROVIDER) != 0x0) &&
			(strncmp(filterRow->Provider, tableRow->Provider, 64) != 0))
		{
			match = false;
		}
		if (match &&
			((filter & COL_DATE) != 0x0) &&
			((filterRow->Date.year != tableRow->Date.year) ||
			(filterRow->Date.month != tableRow->Date.month) ||
			(filterRow->Date.day != tableRow->Date.day))
			)
		{
			match = false;
		}
		if (match &&
			((filter & COL_REV) != 0x0) &&
			(filterRow->Rev != tableRow->Rev)) {
			match = false;
		}
		if (match &&
			((filter & COL_VIEWTIME) != 0x0) &&
			(filterRow->ViewTime != tableRow->ViewTime)) {
			match = false;
		}

		return match;
	}
