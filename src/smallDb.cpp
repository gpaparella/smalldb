// smallDb.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string.h>

#ifndef DATATABLE_H
#include "DataTable.h"
#endif

DataTable* CreateDatabase();
void UpdateDatabase(DataTable* table);

#define BUFFER_SIZE 64

int main(int argc, char* argv[])
{
	// command-line options
	std::string importFile = "";
	std::string select = "";
	std::string order = "";
	std::string filter = "";

	if (argc < 3) {

                std::string peName = argv[0];
		std::cout << "Usage: " << peName << " [-i <IMPORT FILE>|[-s <SELECTED COLUMNS> -o <SORT ORDER> -f <FILTER EXPRESSION>]]" << std::endl;
		return 1;
	}

	// a trailing -i, -s, -o, or -f option with no parameter after it will not be considered
	for (int i = 1; i < argc - 1; ++i) {

		//std::cout << "In arg loop.  i = " << i << ", argc-1 = " << (argc-1) <<
                //             " and argv[i] = " << argv[i] << "\n";

		if (strncmp(argv[i], "-i", 2)==0) {

			importFile = argv[++i]; // buffer;
			//std::cout << "Assigned importFile to " << importFile << "\n";
			//std::cout << "Incremented i.  i = " << i << ", argv[i] is now " << argv[i] << "\n";
		}
		else if (strncmp(argv[i], "-s", 2)==0) {

			select = argv[++i];
			//std::cout << "Assigned select to " << select << "\n";
		}
		else if (strncmp(argv[i], "-o", 2)==0) {

			order = argv[++i];
			//std::cout << "Assigned order to " << order << "\n";
		}
		else if (strncmp(argv[i], "-f", 2)==0) {

			filter = argv[++i];
			//std::cout << "Assigned filter to " << filter << "\n";
		}
	}

	//std::cout << "Constructing DataTable.\n";
	DataTable* table = new DataTable();
	//std::cout << "DataTable constructed.\n";

	if (importFile.length() > 0) {

		std::cout << "Importing file " << importFile << "\n";
		table->ImportFile(importFile);

	} else if ((select.length() > 0) &&
		   (order.length()  > 0) &&
		   (filter.length() > 0)
		  )
	{
		std::cout << table->Query(select, order, filter);
	}

	
	//table->ImportFile("..\\smallDb\\Input.txt");
	//table->ImportFile("..\\smallDb\\Input2.txt");

	//std::cout << table->Query("TITLE,REV,DATE", "DATE,TITLE", "DATE=2014-04-02");

	//DataTable* table = CreateDatabase();
	//UpdateDatabase(table);

	delete(table);

	return 0;
}

DataTable* CreateDatabase() {
	DataTable *table = new DataTable("../obj/Input.txt");
	table->CreateFromFile();
	
	return table;
}

void UpdateDatabase(DataTable* table) {
	
	table->ImportFile("../obj/Input2.txt");
}
